import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatTableModule} from '@angular/material/table';
import { HttpClientModule } from '@angular/common/http'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import {MatIconModule} from '@angular/material/icon';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { NasabahComponent } from './components/nasabah/nasabah.component';
import { TransaksiComponent } from './components/transaksi/transaksi.component';
import { NoPageFoundComponent } from './components/no-page-found/no-page-found.component';
import { DetailsComponent } from './components/details/details.component';
import { KelaminChartComponent } from './components/kelamin-chart/kelamin-chart.component';
import { ProfesiChartComponent } from './components/profesi-chart/profesi-chart.component';
import { IncomeChartComponent } from './components/income-chart/income-chart.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SidebarComponent,
    TopbarComponent,
    NasabahComponent,
    TransaksiComponent,
    NoPageFoundComponent,
    DetailsComponent,
    KelaminChartComponent,
    ProfesiChartComponent,
    IncomeChartComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule, 
    HttpClientModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

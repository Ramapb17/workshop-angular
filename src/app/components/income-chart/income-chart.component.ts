import { Component, OnInit } from '@angular/core';
import { Chart, registerables  } from 'chart.js';
import { NasabahService } from 'src/app/service/nasabah.service';

@Component({
  selector: 'app-income-chart',
  templateUrl: './income-chart.component.html',
  styleUrls: ['./income-chart.component.scss']
})
export class IncomeChartComponent implements OnInit {
  value:any;
  total:any;
  chart:any;
  constructor(private nasabah : NasabahService) {
    Chart.register(...registerables);
   }

  ngOnInit(): void {
    this.nasabah.getIncome().subscribe(res => {
      this.value = res.map((a: any) => a.value);
      this.total = res.map((a:any) => a.total);
      console.log(this.value);
      console.log(this.total);
      this.chart = new Chart('incomeChart', {
        type:'line',
        data:{
          labels:this.value,
          datasets:[
            {
              label: 'Income Demography',
              data: this.total,
              borderWidth:3,
              borderColor:'red'
            },
          ],
        },
      });
    })
  }

}

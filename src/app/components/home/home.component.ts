import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Nasabah } from 'src/app/models/nasabah';
import { NasabahService } from 'src/app/service/nasabah.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  NasabahList : Array<Nasabah> = []
  TableData : any;
  displayedColumns = ['customerId', 'nama', 'nik', 'dob', 'actions']
  
  @ViewChild('paginator1') paginator:MatPaginator | undefined;


  constructor(public nasabahservice : NasabahService) { }

  ngOnInit(): void {
  }
}


import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,  Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NasabahService } from 'src/app/service/nasabah.service';
import { Nasabah } from 'src/app/models/nasabah';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  status:boolean = false;
  id:string = '';
  customer : any;
  provinsi : any;
  kota : any;
  kecamatan : any;
  kelurahan : any;
  kodepos : any;
  jenis_kelamin :any;
  job_list: any;

  // laki_laki : boolean = false;

  constructor(private activateRoute : ActivatedRoute, private nasabah : NasabahService, private router : Router) { }

  ngOnInit(): void {
    this.setMode();
    this.getJenisKelamin();
    this.getProvinsi();
    this.getJenisProfesi();
    if (this.status){
      this.getSingleCustomer(this.id);
    }
  }

  nasabahForm = new FormGroup ({
    CustomerId: new FormControl('', [Validators.required, Validators.minLength(5)]),
    Nama: new FormControl('', [Validators.required, Validators.minLength(5)]),
    NIK: new FormControl('', [Validators.required, Validators.minLength(5)]),
    DOB: new FormControl('', [Validators.required, Validators.minLength(5)]),
    JenisKelamin: new FormControl('', [Validators.required, Validators.minLength(5)]),
    AlamatDomisili: new FormControl('', [Validators.required, Validators.minLength(5)]),
    AlamatKantor: new FormControl('', [Validators.required, Validators.minLength(5)]),
    AlamatKTP: new FormControl('', [Validators.required, Validators.minLength(5)]),
    EmergencyContact: new FormControl('', [Validators.required, Validators.minLength(5)]),
    Periode: new FormControl('', [Validators.required, Validators.minLength(5)]),
    Profesi: new FormControl('', [Validators.required, Validators.minLength(5)]),
    MonthlyIncome: new FormControl('', [Validators.required, Validators.minLength(5)]),
    NamaPerusahaan: new FormControl('', [Validators.required, Validators.minLength(5)]),
    Provinsi: new FormControl('', [Validators.required, Validators.minLength(5)]),
    Kota : new FormControl({ value: '', disabled : true},[Validators.required, Validators.minLength(5)]),
    Kecamatan : new FormControl({ value: '', disabled : true},[Validators.required, Validators.minLength(5)]),
    Kelurahan : new FormControl({ value: '', disabled : true},[Validators.required, Validators.minLength(5)]),
    KodePos : new FormControl({ value: '', disabled : true},[Validators.required, Validators.minLength(5)])
  });

  ProvinsiChange(){
    let a = this.nasabahForm.value.Provinsi;
    this.nasabah.getKota(a).subscribe(res => {
      if(res.data){
        this.kota = res.data;
        this.nasabahForm.patchValue({
          Kota : ' ', 
          Kecamatan : ' ',
          Kelurahan : ' ',
          KodePos : ' '
        });
        this.nasabahForm.controls['Kota'].enable();
        this.nasabahForm.controls['Kecamatan'].disable();
        this.nasabahForm.controls['Kelurahan'].disable();
        this.nasabahForm.controls['KodePos'].disable();
      }
    })
  }

  KotaChange(){
    let a = this.nasabahForm.value.Kota;
    this.nasabah.getKecamatan(a).subscribe(res => {
      if(res.data){
        this.kecamatan = res.data;
        this.nasabahForm.controls['Kecamatan'].enable();
        this.nasabahForm.patchValue({
          Kecamatan : '',
          Kelurahan : '',
          KodePos : ''
        });
        this.nasabahForm.controls['Kelurahan'].disable();
        this.nasabahForm.controls['KodePos'].disable();
      }
    })
  }

  KecamatanChange(){
    let a = this.nasabahForm.value.Kecamatan;
    this.nasabah.getKelurahan(a).subscribe(res => {
      if(res.data){
        this.kelurahan = res.data;
        console.log(res);
        this.nasabahForm.controls['Kelurahan'].enable();
        this.nasabahForm.patchValue({
          Kelurahan : '',
          KodePos : ''
        });
        this.nasabahForm.controls['KodePos'].disable();
      }
    })
  }

  KelurahanChange(){
    let a = this.nasabahForm.value.Kelurahan;
    this.nasabah.getKodePos(a).subscribe(res => {
      if(res.data){
        this.kodepos = res.data;
        this.nasabahForm.controls['KodePos'].enable();
        console.log(res);
      }
    })
  }


  setMode(){
    let id = this.activateRoute.snapshot.paramMap.get('id');
    if (id){
      this.status = true;
      this.id = id;

    } else {
      this.status = false;
    }
    console.log(this.status);
  }

  getProvinsi(){
    this.nasabah.getProvinsi().subscribe(res => {
      this.provinsi = res.data;
    })
  }

  getJenisKelamin(){
    this.nasabah.getJenisKelamin().subscribe(res => {
        this.jenis_kelamin = res.data;
    })
  }

  getJenisProfesi(){
    this.nasabah.getJenisProfesi().subscribe(res => {
      this.job_list = res.data;
    })
  }


  getSingleCustomer(id:string){
    this.nasabah.getSingleNasabah(id).subscribe(res => {
      if (res.data){
        this.customer = res.data[0];
        console.log(this.customer);
        if(this.customer){
          this.nasabah.getKota(this.customer.provinsi).subscribe(res => {
            this.kota = res.data;
            this.nasabah.getKecamatan(this.customer.kota).subscribe(res => {
              this.kecamatan = res.data;
              this.nasabah.getKelurahan(this.customer.kecamatan).subscribe(res => {
                this.kelurahan = res.data;
                this.nasabah.getKodePos(this.customer.kelurahan).subscribe(res => {
                  this.kodepos = res.data;
                  console.log(this.kodepos);
                })
              })
            })
          })
        }
        this.nasabahForm.setValue({
          CustomerId : this.customer.customerId,
          Nama : this.customer.nama,
          NIK : this.customer.nik,
          DOB : this.customer.dob,
          JenisKelamin : this.customer.jenisKelamin,
          AlamatDomisili : this.customer.alamatDomisili,
          AlamatKantor : this.customer.alamatKantor,
          AlamatKTP : this.customer.alamatKTP,
          EmergencyContact : this.customer.emergencyContact,
          Periode : this.customer.periode,
          Profesi : this.customer.profesi,
          MonthlyIncome : this.customer.monthlyIncome,
          NamaPerusahaan : this.customer.namaPerusahaan,
          Provinsi : this.customer.provinsi,
          Kota : this.customer.kota,
          Kecamatan : this.customer.kecamatan,
          Kelurahan : this.customer.kelurahan,
          KodePos : this.customer.kodePos
        })
        this.nasabahForm.controls['Kota'].enable();
        this.nasabahForm.controls['Kecamatan'].enable();
        this.nasabahForm.controls['Kelurahan'].enable();
        this.nasabahForm.controls['KodePos'].enable();
      }
    })
  }


  EditUser(){
    if(!this.status)
    {
      // console.log(this.nasabahForm.value);
        this.nasabah.AddNasabah(this.nasabahForm.value).subscribe(res => {
          if(res.data){
            alert('Data Berhasil Ditambahkan')
            this.nasabahForm.reset();
            this.router.navigate(['/nasabah'])
          }else {
            alert('Maaf, data tidak dapat ditambahkan')
          }
        })
    } 
    else if (this.status)
    {
      // console.log(this.nasabahForm.value);
        this.nasabah.updateNasabah(this.nasabahForm.value).subscribe(res => {
          if(res.data){
            alert('Data Berhasil Diupdate')
            this.nasabahForm.reset();
            this.router.navigate(['/nasabah'])
          }else {
            alert('Maaf, data tidak dapat ditambahkan')
          }
        })
    }
  }

}

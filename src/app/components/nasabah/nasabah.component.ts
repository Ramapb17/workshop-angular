import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Nasabah } from 'src/app/models/nasabah';
import { NasabahService } from 'src/app/service/nasabah.service';

@Component({
  selector: 'app-nasabah',
  templateUrl: './nasabah.component.html',
  styleUrls: ['./nasabah.component.scss']
})
export class NasabahComponent implements OnInit {
  deletestate:boolean = false;
  DCustomer:string ='';
  data:any;
  NasabahList : Array<any> = [];
  TableData : any;
  displayedColumns = ['customerId', 'nama', 'nik', 'dob','monthlyIncome', 'actions']
  
  @ViewChild('paginator1') paginator:MatPaginator | undefined;

  constructor(private nasabahservice : NasabahService,
              private router : Router) { }

  ngOnInit(): void {
      this.nasabahservice.getNasabahList().subscribe((res:any) =>{
      this.NasabahList = res.data;
      this.TableData = new MatTableDataSource<Nasabah>(res.data)
      this.TableData.paginator = this.paginator;
      console.log(this.NasabahList);
    })
  }

  EditData(element:any){
    this.router.navigate(['nasabah/details', element.customerId]);
    this.data = element;
  }

  DeleteData(element:any){
    this.DCustomer = element.customerId;
    this.deletestate = true;
  }

  DeleteCustomer(id : string){
    this.nasabahservice.DeleteNasabah(id).subscribe((res:any) => {
      if(res.status == 200)
      {
        this.NasabahList = this.NasabahList.filter(item => item.customerId !== id);
        console.log(this.NasabahList);
        this.TableData = new MatTableDataSource<Nasabah>(this.NasabahList);
        this.TableData.paginator = this.paginator;
        alert('Data Berhasil Dihapus')
        this.deletestate = false;
      }
      else{
        alert("Data gagal dihapus");
        this.deletestate = false;
      }
    })
  }

  AddDetails(){
    this.router.navigate(['nasabah/details']);
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfesiChartComponent } from './profesi-chart.component';

describe('ProfesiChartComponent', () => {
  let component: ProfesiChartComponent;
  let fixture: ComponentFixture<ProfesiChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfesiChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfesiChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

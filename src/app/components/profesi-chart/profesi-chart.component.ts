import { Component, OnInit } from '@angular/core';
import { Chart, registerables  } from 'chart.js';
import { NasabahService } from 'src/app/service/nasabah.service';

@Component({
  selector: 'app-profesi-chart',
  templateUrl: './profesi-chart.component.html',
  styleUrls: ['./profesi-chart.component.scss']
})
export class ProfesiChartComponent implements OnInit {

  constructor(private nasabah: NasabahService) { 
    Chart.register(...registerables);
  }
  value = [];
  total = [];
  chart:any;
  ngOnInit(): void {
    this.nasabah.getProfesi().subscribe((res:any) => {this.value = res.map((a: any) => a.value);
      this.total = res.map((a:any) => a.total);
      console.log(this.value);
      console.log(this.total);
      this.chart = new Chart('profesiChart', {
        type:'pie',
        data:{
          labels:this.value,
          datasets:[
            {
              label: 'Profesi Demography',
              data: this.total,
              borderWidth:2,
              backgroundColor: this.value.map((a) => this.getRandomColor()),
              borderColor:['white']
            },
          ],
        }, 
      });
    })
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
}

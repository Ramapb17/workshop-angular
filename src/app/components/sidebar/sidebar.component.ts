import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  menu = [
    {
      title: "Dashboard",
      Icon : "Home",
      Link : "/home"
    },
    {
      title: "Nasabah",
      Link : "/nasabah"
    },
    {
      title: "Transaction",
      Link : "/payment"
    }]

  constructor() { }

  ngOnInit(): void {
  }

}

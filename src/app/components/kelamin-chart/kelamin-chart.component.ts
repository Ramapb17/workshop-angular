import { Component, OnInit } from '@angular/core';
import { Chart, registerables  } from 'chart.js';
import { NasabahService } from 'src/app/service/nasabah.service';

@Component({
  selector: 'app-kelamin-chart',
  templateUrl: './kelamin-chart.component.html',
  styleUrls: ['./kelamin-chart.component.scss']
})
export class KelaminChartComponent implements OnInit {
  value = [];
  total = [];
  chart:any;
  constructor(private nasabah:NasabahService) { 
    Chart.register(...registerables);
  }

  ngOnInit(): void {
    this.nasabah.getKelamin().subscribe(res => {
      this.value = res.map((a: any) => a.value);
      this.total = res.map((a:any) => a.total);
      console.log(this.value);
      console.log(this.total);
      this.chart = new Chart('kelaminChart', {
        type:'bar',
        data:{
          labels:this.value,
          datasets:[
            {
              label: 'Kelamin Demography',
              data: this.total,
              borderWidth:2,
              backgroundColor: this.value.map((a) => this.getRandomColor()),
              borderColor:['white']
            },
          ],
        },
      });
    })
  }

  getRandomColor() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KelaminChartComponent } from './kelamin-chart.component';

describe('KelaminChartComponent', () => {
  let component: KelaminChartComponent;
  let fixture: ComponentFixture<KelaminChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KelaminChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KelaminChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

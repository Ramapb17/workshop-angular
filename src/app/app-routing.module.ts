import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './components/details/details.component';
import { HomeComponent } from './components/home/home.component';
import { NasabahComponent } from './components/nasabah/nasabah.component';
import { NoPageFoundComponent } from './components/no-page-found/no-page-found.component';
import { TransaksiComponent } from './components/transaksi/transaksi.component';

const routes: Routes = [
  { path:'', redirectTo: '/home', pathMatch: 'full' },
  { path:'home', component: HomeComponent },
  { path:'nasabah', component: NasabahComponent },
  { path:'nasabah/details/:id', component: DetailsComponent },
  { path:'nasabah/details', component: DetailsComponent },
  { path:'payment', component: TransaksiComponent },
  { path:'**', component: NoPageFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

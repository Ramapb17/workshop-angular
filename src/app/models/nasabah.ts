export interface Nasabah {

    CustomerId : string
    Nama : string
    DOB : string
    NIK : string
    JenisKelamin : string
    AlamatDomisili : string
    AlamatKantor : string
    AlamatKTP : string
    EmergencyContact : string
    Periode : string
    Profesi : string
    MonthlyIncome : string
    NamaPerusahaan : string
    Provinsi : string
    Kota : string
    Kecamatan : string
    Kelurahan : string
    KodePos : string

}

export interface Response {
    
}

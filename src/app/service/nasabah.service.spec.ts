import { TestBed } from '@angular/core/testing';

import { NasabahService } from './nasabah.service';

describe('NasabahService', () => {
  let service: NasabahService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NasabahService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, pipe, throwError } from 'rxjs';
import { Nasabah } from '../models/nasabah';

@Injectable({
  providedIn: 'root'
})
export class NasabahService {
  endpoint:string =  "https://localhost:7135/api/Customer/";
  header = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  getNasabahList():Observable<any>{
    
    return this.http.get(this.endpoint + "GET", {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getSingleNasabah(id:string):Observable<any>{
    
    return this.http.get(this.endpoint + id, {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  AddNasabah(nasabah : Nasabah):Observable<any>{
    return this.http
      .post(this.endpoint + "Create", nasabah)
      .pipe(catchError(this.handleError));
  }

  updateNasabah(nasabah : Nasabah):Observable<any>{
    return this.http
      .put(this.endpoint + "Update", nasabah)
      .pipe(catchError(this.handleError));
  }

  DeleteNasabah(id:string):Observable<any>{
    
    return this.http.delete(this.endpoint + 'Delete/' + id, {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getKelamin():Observable<any>{
    return this.http.get(this.endpoint + "GET/Kelamin", {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getProfesi():Observable<any>{
    return this.http.get(this.endpoint + "GET/Profesi", {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getIncome():Observable<any>{
    return this.http.get(this.endpoint + "GET/Income", {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  
  getProvinsi():Observable<any>{
    return this.http.get(this.endpoint + "GET/Provinsi", {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getJenisProfesi():Observable<any>{
    return this.http.get(this.endpoint + "GET/JenisProfesi", {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getJenisKelamin():Observable<any>{
    return this.http.get(this.endpoint + "GET/JenisKelamin", {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getKota(provinsi : string):Observable<any>{
    return this.http.get(this.endpoint + "GET/Kota/" + provinsi, {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getKecamatan(kota : string):Observable<any>{
    return this.http.get(this.endpoint + "GET/Kecamatan/" + kota, {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getKelurahan(kecamatan : string):Observable<any>{
    return this.http.get(this.endpoint + "GET/Kelurahan/" + kecamatan, {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }

  getKodePos(kelurahan : string):Observable<any>{
    return this.http.get(this.endpoint + "GET/KodePos/" + kelurahan, {headers: this.header}).pipe(
      map((res:any) => {
        return res || {}
      }), catchError(this.handleError))
  }


  

  //Error handling
  handleError(error: HttpErrorResponse) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      //client-side error
      msg = error.error.message;
    } else {
      //server-side error
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(msg);
  }
}